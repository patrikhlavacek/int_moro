import React from 'react';
import PropTypes from 'prop-types';

const  TableCell = props =>{
        return(
            <td>{props.content}</td>
        );
}

TableCell.propTypes = {
    content: PropTypes.node
}

export default TableCell;