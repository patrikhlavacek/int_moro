import React from 'react';
import PropTypes from 'prop-types';

class AddUser extends React.Component{

    state = {newUser: ''}

    handleChange(event){
        this.setState({newUser: event.target.value})
    }

    render(){
        return(
            <div>
                <input type='text' onChange={this.handleChange.bind(this)} />
                <button onClick={this.props.onUserAdded(this.state.newUser)}>Přidat</button>
            </div>
        );
    }
}

AddUser.propTypes = {
    onUserAdded: PropTypes.func,

}
export default AddUser;