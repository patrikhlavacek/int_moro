package db.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    private Random rand = new Random();

    List<User> getUsers (){
        return userRepository.findAll();
    }

    @Transactional
    User saveUser (String name){
        User newUser = new User(name, String.valueOf(rand.nextInt(8000)), String.valueOf(rand.nextInt(8000)));
        return userRepository.save(newUser);
    }

    @Transactional
    User editUser (Long id, String name){
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()){
            User editedUser = user.get();
            editedUser.setName(name);
            userRepository.save(editedUser);
            return editedUser;
        } else {
            throw new EmptyResultDataAccessException(1);
        }
    }

    @Transactional
    boolean deleteUser (Long id){
        userRepository.deleteById(id);
        return !userRepository.existsById(id);
    }
}