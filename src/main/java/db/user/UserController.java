package db.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("api/")
    public @ResponseBody List<User> getUsers() {
        return userService.getUsers();
    }

    @PostMapping("api/newUser/{name}")
    public @ResponseBody User newUser
           (@PathVariable(value = "name") final String name) {
        return userService.saveUser(name);
    }

    @PostMapping("api/editUser/{id}")
    public @ResponseBody User editUser
            (@PathVariable(value = "id") final Long id,
                                         final String newName) {
        return userService.editUser(id, newName);
    }

    @GetMapping("api/deleteUser/{id}")
    public @ResponseBody boolean deleteUser
            (@PathVariable(value = "id") final Long id) {
        return userService.deleteUser(id);
    }
}