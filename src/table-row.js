import React from 'react';
import PropTypes from 'prop-types';

import TableCell from './table-cell.js';

class TableRow extends React.Component{

        state = {inEdit: false, newUserName: ''}

        handleChange(event){
            this.setState({newUserName: event.target.value})
        }

        handleSave(){
            let editedUser = {id: this.props.user.id, name: this.state.newUserName}
            this.props.onUserEdited(editedUser);
            this.setState({inEdit: false});
        }

        render() {
        if(this.state.inEdit){
            return(
                <tr>
                  <TableCell content={this.props.user.id}/>
                  <TableCell content={<input type='text' onChange={this.handleChange.bind(this)} />}/>
                  <TableCell content={<button onClick={this.handleSave.bind(this)}>Uložit</button>}/>
                  <TableCell content={<button onClick={() => this.setState({inEdit: false})}>Zrušit</button>}/>
                </tr>
            );
        }else{}
            return(
              <tr>
                <TableCell content={this.props.user.id}/>
                <TableCell content={this.props.user.name}/>
                <TableCell content={<button onClick={this.props.onUserDeleted(this.props.user)}>Smazat</button>}/>
                <TableCell content={<button onClick={() => this.setState({inEdit: true})}>Upravit</button>}/>
              </tr>
            );
        }
}

TableRow.propTypes = {
    onUserDeleted: PropTypes.func,
    onUserEdited: PropTypes.func,
    user: PropTypes.object
}

export default TableRow;