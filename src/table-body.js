import React from 'react';
import PropTypes from 'prop-types';

import TableRow from './table-row.js';

const  TableBody = props =>{

    return (
       <tbody>
           {mapUsers(props)}
       </tbody>
    );

}

const mapUsers = (props) => {
    if(props.users){
        return props.users.map((user, index) =>
                                              (<TableRow key={user.id} user={user}
                                               onUserDeleted={(user) => props.onUserDeleted(user)}
                                               onUserEdited={(editedUser) => props.onUserEdited(editedUser)}/>));

    }
}

TableBody.propTypes = {
    users: PropTypes.array,
    onUserEdited: PropTypes.func,
    onUserDeleted: PropTypes.func
}
export default TableBody;