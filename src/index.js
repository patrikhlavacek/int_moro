import React from 'react';
import ReactDOM from 'react-dom';

import TableHeader  from './table-header.js';
import TableBody  from './table-body.js';
import AddUser  from './add-user.js';

const request = require('superagent');
class UserList extends React.Component{

    state = {users: []};


    componentDidMount() {
        request.get("http://localhost:8080/api/").then(res => {
            this.setState({users: res.body});
        })
    }

    handleUserEdited(editedUser) {
        request.post("http://localhost:8080/api/editUser/" + editedUser.id + "?newName=" + editedUser.name)
             .then(res => {
                const newUsers = this.state.users.slice();
                const index = this.state.users.findIndex(this.findUserIndex, editedUser);
                newUsers[index] = {id: res.body.id, name: res.body.name};
                this.setState({users: newUsers});
             })
    }

    handleUserDeleted(user) {
        request.get("http://localhost:8080/api/deleteUser/" + user.id)
                     .then(res => {
                        if(res.body){
                           const newUsers = this.state.users.slice();
                           const index = this.state.users.findIndex(this.findUserIndex, user);
                           newUsers.splice(index,1)
                           this.setState({users: newUsers});
                        }
                     })
    }

    handleUserAdded(userName) {
        request.post("http://localhost:8080/api/newUser/" + userName)
                     .then(res => {
                        const newUsers = this.state.users.slice();
                        newUsers.push({id: res.body.id, name: res.body.name})
                        this.setState({users: newUsers});
                     })

    }

    findUserIndex(element){
        return element.id === this.id;

    }

    render() {
    return(
    <div>
        <table>
          <TableHeader/>
          <TableBody users={this.state.users}
                     onUserDeleted={(user) => this.handleUserDeleted.bind(this, user)}
                     onUserEdited={(editedUser) => this.handleUserEdited(editedUser)} />
        </table>
        <AddUser onUserAdded={(userName) => this.handleUserAdded.bind(this, userName)}/>
    </div>
    );
    }
}


// ========================================

ReactDOM.render(
  <UserList />,
  document.getElementById('root')
);
